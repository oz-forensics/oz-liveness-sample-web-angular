import { Injectable } from '@angular/core';

export type OzLiveness = {
    open: (options?: any) => Promise<void>
};

export interface ICustomWindow extends Window {
    OzLiveness: OzLiveness;
}

function getWindow (): any {
    return window;
}

@Injectable()
export class OzLivenessService {
    get OzLiveness (): OzLiveness {
        return getWindow().OzLiveness;
    }
}