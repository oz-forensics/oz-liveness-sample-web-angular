import { Component, NgZone } from '@angular/core';
import { OzLivenessService } from './OzLiveness.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  foto: string = '';
  eventSessionId: string = '';
  analysesResult: any = null;
  errorMessage: string = '';

  constructor(
    private ozModule: OzLivenessService,
    private ngZone: NgZone,
  ) {}

  onOpenLiveness = () => {
    this.reset();

    this.ozModule.OzLiveness.open({
      on_submit: (result: any) => console.log('on_submit', result),
      on_result: (result: any) => console.log('on_result', result),
      on_close: (result: any) => console.log('on_close', result),

      on_error: (error: any) => this.ngZone.run(() => {
        console.error('on_error', error);
        this.eventSessionId = error.event_session_id;
        this.errorMessage = `${error}`;
      }),

      on_complete: (result: { analyses: any }) => this.ngZone.run(() => {
        console.log('on_complete', result);
        this.analysesResult = result.analyses;
      }),

      on_capture_complete: (result: { best_frame: string, event_session_id: string }) => this.ngZone.run(() => {
        console.log('on_capture_complete', result);
        this.foto = result.best_frame;
        this.eventSessionId = result.event_session_id;
      }),
    });
  }

  getKeys(obj: any): string[] {
    return Object.keys(obj);
  }

  reset = () => {
    this.foto = '';
    this.eventSessionId = '';
    this.analysesResult = null;
    this.errorMessage = '';
  }
}
