import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { OzLivenessService } from './OzLiveness.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
  ],
  providers: [OzLivenessService],
  bootstrap: [AppComponent]
})
export class AppModule { }
